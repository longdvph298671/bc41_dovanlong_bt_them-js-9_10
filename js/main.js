
var DSSV = [];
// lấy dữ liệu từ localStorge và convert lại method;
function loadDSSV(){
    var dataJson = localStorage.getItem('DSSV_LOCAL');
    
    if(dataJson != null) {
        var dataArr = JSON.parse(dataJson);
        DSSV = dataArr.map(function(item){
            var sv = new SinhVien (
                item.maSV,
                item.tenSV,
                item.emailSV,
                item.passSV,
                item.ngaySinh,
                item.khoaHoc,
                item.diemToan,
                item.diemLy,
                item.diemHoa
            );
            return sv;
        });
        renderDSSV(DSSV);
    }
}
loadDSSV();

function themSV() {
    var sv = layThongTinTuForm();
    var temp = true;
    for(var i = 0; i < DSSV.length; i++) {
        // kiểm tra mã sv trùng ko
        if(sv.maSV == DSSV[i].maSV) {
            temp = false;
            break;
        }
    }
    // nếu ko trùng thêm sv;
    if(temp){
        DSSV.push(sv);
    
        var dssvJson = JSON.stringify(DSSV);
        localStorage.setItem('DSSV_LOCAL', dssvJson);
    
        renderDSSV(DSSV);
        resetForm();
    }
    else {
        alert('Mã sinh viên đã tồn tại')
    }
}

function xoaSV(idSV) {
    var viTri = timVitri(idSV, DSSV);
    if(viTri != -1){
        DSSV.splice(viTri, 1);

        // update lại localStorage sau khi xoá
        var dssvJson = JSON.stringify(DSSV);
        localStorage.setItem('DSSV_LOCAL', dssvJson);

        loadDSSV();
        
    }
}

function suaSV(idSV) {
    var viTri = timVitri(idSV, DSSV);
    if(viTri != -1){
        var sv = DSSV[viTri];
        document.getElementById('txtMaSV').disabled = true;
        inThongTinLenForm(
            sv.maSV,
            sv.tenSV,
            sv.emailSV,
            sv.passSV,
            sv.ngaySinh,
            sv.khoaHoc,
            sv.diemToan,
            sv.diemLy,
            sv.diemHoa
        )
    }
}

function capNhatSV() {
    var idSV = document.getElementById('txtMaSV').value;
    var temp = false;
    // kiểm tra mã SV cần sửa đã có hay chưa
    for(var i = 0; i < DSSV.length; i++){
        if(idSV == DSSV[i].maSV){
            temp = true;
            break;
        }
    }
    //nếu có thì cập nhật lại
    if(temp == true){
        var viTri = timVitri(idSV, DSSV);
        var sv = layThongTinTuForm();
        // DSSV[viTri] = sv; or
        DSSV.splice(viTri, 1, sv);
        
        // update lại localStorage sau khi sửa
        var dssvJson = JSON.stringify(DSSV);
        localStorage.setItem('DSSV_LOCAL', dssvJson);
        
        loadDSSV();
        resetForm();
    }
    else {
        alert('SV cần sửa không tồn tại')
    }
}

function saerchSV() {
    var keyWord = document.getElementById('txtSearch').value.toLowerCase();
    var result = [];
    for(var i = 0; i < DSSV.length; i++) {
        var nameSv = DSSV[i].tenSV.toLowerCase();
        if(nameSv.search(keyWord) != -1){
            result.push(DSSV[i]);
        }
    }
    renderDSSV(result);
}


