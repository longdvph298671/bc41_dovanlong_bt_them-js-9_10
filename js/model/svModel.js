
function SinhVien(
    maSV,
    tenSV,
    emailSV,
    passSV,
    ngaySinh,
    khoaHoc,
    diemToan,
    diemLy,
    diemHoa
) {
    this.maSV = maSV;
    this.tenSV = tenSV;
    this.emailSV = emailSV;
    this.passSV = passSV;
    this.ngaySinh = ngaySinh;
    this.khoaHoc = khoaHoc;
    this.diemToan = diemToan;
    this.diemLy = diemLy;
    this.diemHoa = diemHoa;
    this.tinhDTB = function () {
        var dtb = (diemToan*1 + diemLy*1 + diemHoa*1)/3;
        return dtb.toFixed(2);
    };
}