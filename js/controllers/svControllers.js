
function layThongTinTuForm() {
    var masv = document.getElementById('txtMaSV').value;
    var tensv = document.getElementById('txtTenSV').value;
    var email = document.getElementById('txtEmail').value;
    var pass = document.getElementById('txtPass').value;
    var ngaysinh = document.getElementById('txtNgaySinh').value;
    var khoahoc = document.getElementById('khSV').value;
    var diemtoan = document.getElementById('txtDiemToan').value;
    var diemly = document.getElementById('txtDiemLy').value;
    var diemhoa = document.getElementById('txtDiemHoa').value;

    return new SinhVien (
        masv,
        tensv,
        email,
        pass,
        ngaysinh,
        khoahoc,
        diemtoan,
        diemly,
        diemhoa
    )
}

function inThongTinLenForm(
    masv,
    tensv,
    email,
    pass,
    ngaysinh,
    khoahoc,
    diemtoan,
    diemly,
    diemhoa
    ) {
        document.getElementById('txtMaSV').value = masv;
        document.getElementById('txtTenSV').value = tensv;
        document.getElementById('txtEmail').value = email;
        document.getElementById('txtPass').value = pass;
        document.getElementById('txtNgaySinh').value = ngaysinh;
        document.getElementById('khSV').value = khoahoc;
        document.getElementById('txtDiemToan').value = diemtoan
        document.getElementById('txtDiemLy').value = diemly;
        document.getElementById('txtDiemHoa').value = diemhoa;
    }

function renderDSSV(arrSV) {
    var contentHTML = '';
    for(var i = 0; i < arrSV.length; i++){
        var contentTr = `<tr>
                            <td>${arrSV[i].maSV}</td>
                            <td>${arrSV[i].tenSV}</td>
                            <td>${arrSV[i].emailSV}</td>
                            <td>${arrSV[i].ngaySinh}</td>
                            <td>${arrSV[i].khoaHoc}</td>
                            <td>${arrSV[i].tinhDTB()}</td>
                            <td class='row'>
                            <button onclick="suaSV('${arrSV[i].maSV}')" class="btn btn-warning">Sửa</button>
                            <button onclick="xoaSV('${arrSV[i].maSV}')" class="btn btn-danger">Xoá</button>
                            </td>
                        </tr>`
        contentHTML += contentTr;
    }
    document.getElementById('tbodySinhVien').innerHTML = contentHTML;
}

function timVitri(idSV, arrSV){
    var viTri = -1;
    for(var i = 0; i < arrSV.length; i++) {
        if(arrSV[i].maSV == idSV){
            viTri = i;
        }
    }
    return viTri;
}

function resetForm(){
    document.getElementById('txtMaSV').disabled = false
    document.getElementById('formQLSV').reset();
}

